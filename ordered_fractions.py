from fractions import Fraction

approx, target = Fraction(2/5),Fraction(3/7)

# as 2/5 = 2*(200000-1)/5*(200000-1) = 399998/500000 and 2/5 < 399999 / 500000 < 3/7
for d in range(int(1e6)-5, int(1e6)+1):
    for n in range(int(d*target)+1, int(d*approx)-1, -1):
        cand = Fraction(n,d)
        if cand < approx:
            break
        if cand < target:
            approx = cand
print(approx.numerator)
