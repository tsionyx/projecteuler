def prime(max):
    return [x for x in range(2, max+1) if all((x%y for y in range(2,1+int(x**0.5))))]

def max_degree(n, m):
    q = 0
    while n % m == 0:
        n /= m
        q += 1
    return q
 
def factorize(n):
    return {p:max([max_degree(i,p) for i in range(2, n+1)]) for p in prime(n)}

def product(dict):
    p = 1
    for i in dict:
        p *= i**dict[i]
    return p

print(product(factorize(20)))