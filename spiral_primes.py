def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    return all((n%y for y in range(2, int(n**.5)+1)))

def corners(n):
    if n==1: 
        return 1
    return [n**2 - 3*n +3, n**2 - 2*n +2, n**2 - n +1]
    
n = 1
primes = 0
while True:
    n += 2
    total = 2*n-1
    primes += len([1 for corner in corners(n) if prime(corner)])
    if total/primes > 10:
        print(n)
        break