def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    return is_prime

'''
    Sum of all digits of n-pandigital number is n*(n+1)/2.
    If sum of digits is divided by 3 then number itself divided by 3.
    Then prime pandigital numbers can only be 4- and 7-digital
'''
from time import time
start_time = time()

from itertools import permutations
print(next(i for i in(int(''.join([str(j) for j in i])) for n in (7,4)\
           for i in permutations(range(n,0,-1))) if prime(i)))
            
print(time() - start_time, "seconds") 