def is_palindrome(num):
    s = str(num)
    return s == s[::-1]
    # abcde:
    # abc == edc
    # abcdef:
    # abc == fed
    # return s[:int((len(s)+1)/2)] == s[:int((len(s)-1)/2):-1]

import itertools
digits = 3
range = range(10**digits - 1, 10**(digits - 1), -1)
print(max([i[0]*i[1] for i in itertools.product(range, range) if is_palindrome(i[0]*i[1])]))

#for i in itertools.product(range, range):
#    if is_palindrome(i[0]*i[1]):
#        print("%d * %d = %d" % (i[0], i[1], i[0]*i[1])) 
#        break
        
        
        
     