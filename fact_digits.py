def sum_digits(n):
	s = 0
	while n:
		n, r = divmod(n,10)
		s += r
	return s

from math import factorial

print(sum_digits(factorial(100)))