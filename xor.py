with open("cipher1.txt", 'r') as cipher:
	ciphered = list(map(int,cipher.readline().rstrip().split(',')))
	
def is_legal_symbol(ch):
	return 32 <= ch <= 90 or 97 <= ch <= 122
	
from itertools import product
keys = product(range(ord('a'), ord('z')+1),repeat=3)

for key in keys:
	if all(is_legal_symbol(c^key[i%3]) for i,c in enumerate(ciphered)):
		print(key)
		print(''.join(chr(c^key[i%3]) for i,c in enumerate(ciphered)))
		print(sum((c^key[i%3]) for i,c in enumerate(ciphered)))