spells = {0:'', 1:'one', 2:'two', 3:'three', 4:'four', 5: 'five', 6:'six', 7:'seven', 8:'eight',9:'nine',10:'ten',
11: 'eleven', 12:'twelve', 13:'thirteen', 14: 'fourteen', 15: 'fifteen', 16:'sixteen', 17: 'seventeen', 18: 'eighteen', 19:'nineteen',
20: 'twenty', 30:'thirty', 40:'forty', 50:'fifty', 60:'sixty', 70:'seventy',80:'eighty',90:'ninety',100:'hundred', 1000:'thousand'}

def spell_the_number(number):
    if number==1000:
        return spells[1]+spells[1000]
    elif number >= 100:
        hundreds = int(number/100)
        return spells[hundreds]+spells[100]+('and'+spell_the_number(number%100) if number%100 else '')
    elif number>20:
        tens = int(number/10)
        return spells[tens*10]+spell_the_number(number%10)
    else:
        return spells[number]
        
print(sum([len(spell_the_number(n)) for n in range(1001)]))
#print([spell_the_number(n) for n in range(1001)])