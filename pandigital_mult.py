'''
    each product of l-digit number with numbers from 1 to 9 contains l or l+1 digits
    So for given n we can try to solve equation:
        l*x+(n-x)*(l+1) = 9
        or
        l = (9 - n + x)/n
    where l is a length of start number and x is a number of l-digit products (0 < x <= n)
    
    n = 9 => 
        l = x/9 => l = 1, x = 9, the only possible solution is
        1*(1..9) = 1234556789 - the least 9-pandigital

    n = 8 =>
        l = (x + 1)/8 => l = 1, x = 7, 
        no such 1-digit number that produce 1-digit when multiply by 7 
        and 2-digit when multiply by 8

    n = 7 =>
        l = (x + 2)/7 => l = 1, x = 5, 
        no such 1-digit number that produce 1-digit when multiply by 5 
        and 2-digit when multiply by 6
    
    n = 6 =>
        l = (x + 3)/6 => l = 1, x = 3, 
        the only 1-digit number that produce 1-digit when multiply by 3
        and 2-digit when multiply by 4 is 3, but 3*(1..6) = 369121518 is not pandigital
    
    n = 5 =>
        l = (x + 4)/5 => l = 1, x = 1, 
        the only 1-digit number that produce 1-digit when multiply by 3
        and 2-digit when multiply by 4 is 3, but 3*(1..6) = 369121518 is not pandigital
'''

temp = [(n,(9-n+x)/n,x) for n in range(9,1,-1) for x in range(n,0,-1)]
digits = [(n,int(l),x) for n, l, x in temp if (int(l)*n + n == x + 9)]
#[print("%d %d-digit numbers and %d %d-digit numbers" %(x,l,n-x,l+1)) for n,l,x in digits]

def is_pandigital(s):
    return len("".join(set(s)).replace('0','')) == 9
           
import math           
borders = {n:(m1, m2) for n, m1, m2 in\
          [(n, math.ceil(10**l / min(10, x+1)), math.ceil(10**l/x)) for n,l,x in digits]\
          if m1 < m2}

res = []
for n,(m1,m2) in borders.items():
    for i in range(m1, m2):
        s = ''
        for j in range(1,n+1):
            s += str(i*j)
        if(is_pandigital(s)):
                res.append(int(s))    

print(max(res))

'''   
for n,l,x in digits:
    print("1..%d * [%d.. %d]" %(n, math.ceil(10**l / min(10, x+1)), math.ceil(10**l/x))) 
'''