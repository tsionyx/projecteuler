with open('triangles.txt') as ftriangles:
    triangles = [[trs[0:2], trs[2:4], trs[4:6]] for trs in [tuple(map(int, line.strip().split(','))) for line in ftriangles.readlines()]]

def is_point_inside(s, a, b, c):
    as_x, as_y = s[0]-a[0], s[1]-a[1]
    s_ab = (b[0]-a[0])*as_y-(b[1]-a[1])*as_x > 0

    if ((c[0]-a[0])*as_y-(c[1]-a[1])*as_x > 0) == s_ab:
        return False
    if((c[0]-b[0])*(s[1]-b[1])-(c[1]-b[1])*(s[0]-b[0]) > 0) != s_ab:
        return False
    return True
    
print(sum(1 for trs in triangles if is_point_inside((0,0),*trs)))