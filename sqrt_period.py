def sqrt(N):
    n=N**.5
    integer = int(n)
    fractions = []
    # sqrt(23) = 4 + (sqrt(23)-4)/1
    start = [0]*3
    start[2] = N - integer**2
    if start[2] == 0:
        return [integer, []]
    start[0] = int((n + integer)/start[2])
    start[1] = start[0]*start[2] - integer
    fractions.append(start[0])
    
    i, r, d = start
    while True:
        d = (N - r**2) // d
        i = int((n + r)/d)
        r = i*d - r
        if [i,r,d] == start:
            break
        fractions.append(i)
    
    return [integer, fractions]
    
    
#[print("sqrt({})=[{};{}], period={}".format(i,s[0],tuple(s[1]),len(s[1]))) for i,s in [(i, sqrt(i)) for i in range(14)]]

print(sum(1 for i in range(10001) if len(sqrt(i)[1]) % 2))