loops = {1:1,2:1,145:1,45580:1, 
         169:3, 133660:3, 1445:3,
         178:2, 13456:2,
         278:2, 23456:2}
loops2 = {1:1,2:1,145:1,40585:1,169:3,363601:3,1454:3,871:2,45361:2,872:2,45362:2}
loopers = set([1,2,145,40585,169,363601,1454,871,45361,872,45362])

from math import factorial
df = [factorial(i) for i in range(10)]
     
def fact_chain(n):
    l = 0
    while n not in loops2:
        #print("    ",n)
        n = sum(df[int(c)] for c in str(n))
        l += 1
    return l + loops2[n]

def zero_pref(s):
    if s=='0':
        return 100
    return ord(s[0])
def fact_chain_rec(n):
    m = n
    n = int(''.join(sorted(str(n),key=zero_pref)))
    #print("    ",m,n)
    if m not in loopers and n in loops:
        return loops[n]+1
    if n in loops:
        return loops[n]
    if n not in loops:
        loops[n] = fact_chain_rec(sum(df[int(c)] for c in str(n)))
        return loops[n]+1
    
# print(sum(1 for i in range(3, int(1e6)) if fact_chain(i)==5]
# sum = 0
# for i in range(69, int(1e6)):
    # if fact_chain(i)==5:
        # print(i)
        
# print(fact_chain_rec(69),fact_chain(69))
# print(fact_chain_rec(78),fact_chain(78))
# print(fact_chain_rec(540),fact_chain(540))
# print(fact_chain_rec(45361),fact_chain(45361))
# print(fact_chain_rec(45163),fact_chain(45163))
# print(fact_chain_rec(145),fact_chain(145))
# print(fact_chain_rec(154),fact_chain(154))
#print(fact_chain(14558))

 
from time import time
start_time = time()
print(all(fact_chain_rec(i) for i in range(3,1000000)))
print(time() - start_time, "seconds") 

start_time = time()
print(all(fact_chain(i) for i in range(3,1000000)))
print(time() - start_time, "seconds") 