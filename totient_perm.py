def add_to_factor_dict(factors, new):
    if new in factors:
        factors[new] += 1
    else:
        factors[new] = 1
    return factors
 
factor_cache = {} 
def factor2(n):
    assert n > 1, "What r u going 2 do?"
    
    if n in factor_cache:
        return factor_cache[n]
    
    factors = {}
    found = False
    for y in range(2,int(n**0.5)+1):
        if n%y == 0:
            found = True
            factors = add_to_factor_dict(factors, y)
            for other in factor2(n // y):
                factors = add_to_factor_dict(factors, other)
            break
    if not found:
        factors[n]=1
            
    factor_cache[n] = factors
    return factors    
    
    
 
def factor(N):
    assert N > 1, "What r u going 2 do?"
    
    factors = {}
    n = N
    while(n>1):
        found = False
        for y in range(2,int(N**0.5)+1):
            if n%y == 0:
                found = True
                if y in factors:
                    factors[y] += 1
                else:
                    factors[y] = 1
                n //= y
                break
        if not found:
            factors[n]=1
            break
    return factors    
    
def totient(n):
    for f in tuple(factor2(n)):
        n = n//f * (f-1)
    return n        

def is_totient_perm(n):
    return sorted(str(n))==sorted(str(totient(n)))

print(is_totient_perm(87109))

'''
min = 2
mini = 2
for i in range(2,10**7+1):
   if is_totient_perm(i):
        print(i, totient(i),i/totient(i),min,mini)
        if i/totient(i) < min:
            min = i/totient(i)
            mini = i
print(mini)
'''

def sieve(max):
    if max < 3:
        raise ValueError("Use sieve with at least 3 as maximum number")
    res = [2]
    for n in range(3,max):
        if all(n%y for y in range(2,int(n**0.5)+1) if y in res):
            res.append(n)            
    return res
    
    
cands = sieve(5000)

min = 2
mini = 2
for i in cands:
    for j in cands:
        N = i*j
        if i == j or N > 10**7:
            continue
        
        if is_totient_perm(N):
            print(N, totient(N),N/totient(N),min,mini)
            if N/totient(N) < min:
                min = N/totient(N)
                mini = N
print(mini) 
