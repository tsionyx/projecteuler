 
from time import time
start_time = time()

'''
    Put your code here
'''
 
from time import time
start_time = time()
def is_permuted(n):
    s = set(str(n))
    return all((set(str(i*n))==s for i in range(2,7)))
    
print(next((i for n in range(2, 9) for i in range(10**(n-1)+2, 10**n//6 + 1) if is_permuted(i))))

print(time() - start_time, "seconds") 
