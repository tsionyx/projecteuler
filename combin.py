from math import factorial
def combinations(n, r):
    return factorial(n)//(factorial(r)*factorial(n-r))

print(len([1 for n in range(1,101) for r in range(0,n+1) if combinations(n,r)>1000000]))