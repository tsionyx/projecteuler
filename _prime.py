cache = {}
def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    if n in cache:
        return cache[n]
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    cache[n] = is_prime
    return is_prime
    
    
def naive_factor(N):
    assert N > 1, "What r u going 2 do?"
    
    factors = {}
    n = N
    while(n>1):
        found = False
        for y in range(2,int(N**0.5)+1):
            if n%y == 0:
                found = True
                if y in factors:
                    factors[y] += 1
                else:
                    factors[y] = 1
                n //= y
                break
        if not found:
            factors[n]=1
            break
    return factors
    
factor_cache = {} 
def factor(n):
    assert n > 1, "What r u going 2 do?"
    
    if n in factor_cache:
        return factor_cache[n]
    
    factors = {}
    found = False
    for y in range(2,int(n**0.5)+1):
        if n%y == 0:
            found = True
            factors = add_to_factor_dict(factors, y)
            for other in factor(n // y):
                factors = add_to_factor_dict(factors, other)
            break
    if not found:
        factors[n]=1
            
    factor_cache[n] = factors
    return factors       
if __name__ == '__main__':
    print(naive_factor(100000000))
         