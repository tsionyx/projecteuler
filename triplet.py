def isPythagorean(a, b, c):
    return a**2 + b**2 == c**2


def find_triplet(sum):
    #from itertools import product    
    #return [(a, b, sum -(a+b)) for (a,b) in product(r, r) if isPythagorean(a,b,sum-(a+b))]
    
    for a in range(1, sum):
        for b in range(a+1, sum):
            if isPythagorean(a,b,sum-(a+b)):
                return (a,b,sum-(a+b))        
    

tr = find_triplet(1000)    
print(tr[0]*tr[1]*tr[2])