def period_length(n):
    while n%2 == 0:
        n //= 2
    while n%5 == 0:
        n //= 5
    return discrete_ten_log(n)
           
def discrete_ten_log(base):
    if base == 1:
        return 0
    log, ten = 1, 10
    while True:
        if ten % base == 1:
            return log
        log, ten = log + 1, ten * 10
        
print(max([(i,period_length(i)) for i in range(2, 1000)],key=lambda x:x[1]))
