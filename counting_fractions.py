def add_to_factor_dict(factors, new):
    if new in factors:
        factors[new] += 1
    else:
        factors[new] = 1
    return factors
 
factor_cache = {} 
def factor(n):
    assert n > 1, "What r u going 2 do?"
    
    if n in factor_cache:
        return factor_cache[n]
    
    factors = {}
    found = False
    for y in range(2,int(n**0.5)+1):
        if n%y == 0:
            found = True
            factors = add_to_factor_dict(factors, y)
            for other in factor(n // y):
                factors = add_to_factor_dict(factors, other)
            break
    if not found:
        factors[n]=1
            
    factor_cache[n] = factors
    return factors
    
def totient(n):
    for f in tuple(factor(n)):
        n = n//f * (f-1)
    return n
    
print(sum(totient(i) for i in range(2,10**6+1)))