def triangle(n):
    return int(n*(n+1)/2)
    
def divisors(n):
    return len([d for d in range(1,n+1) if n%d==0])

def generate_triangles():
    n = 1
    while(True):
        yield triangle(n)
        n+=1

tr = generate_triangles()
while(True):
    i = next(tr)
    d = divisors(i)
    print(i,d)
    if d > 500:
        break
