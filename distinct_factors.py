def distinct_factors(N):
    assert N > 1, "What r u going 2 do?"
    
    factors = set()
    n = N
    while(n>1):
        found = False
        for y in range(2,int(N**0.5)+1):
            if n%y == 0:
                found = True
                factors.add(y)
                while(n%y==0): 
                    n //= y
                break
        if not found:
            return len(factors)+1
    return len(factors)
    
    
CONSECUTIVE, DISTINCT, i = 4,4,645

while True:
    if all(distinct_factors(i+j)==DISTINCT for j in range(CONSECUTIVE)):
        print(i)
        break
    i += 1
    