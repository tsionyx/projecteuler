from itertools import product
def get_rectangles_count(N, M):
   return sum(p[0]*p[1] for p in product(range(1,N+1),range(1,M+1)))

target = 2000000

m = 1000
nTarget, mTarget = 0,0

N = 1
while True:
    if N*(N-1)//2 > target:
        break     
    M = 1
    while True:
        max_count_upper = get_rectangles_count(N, M)
        if  max_count_upper > target:
            if max_count_upper - target < m:
                m = max_count_upper - target
                nTarget, mTarget = N, M
            max_count_lower = get_rectangles_count(N, M-1)
            if abs(target - max_count_lower) < m:
                m = abs(max_count_lower - target)
                nTarget, mTarget = N, M-1
            break
        M+=1
    N += 1
    
print(nTarget * mTarget)