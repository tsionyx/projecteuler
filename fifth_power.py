POWER = 5
BASE = 10

def find_max_digits(power):
    max_digit_power = (BASE-1)**power
    digits = 2
    while(max_digit_power*digits >= BASE**(digits)):
        digits += 1
    return digits
    
def find_upper(digits, power):
    for i in range(1, BASE):
        if i**power > BASE ** digits:
            return i-1
    return BASE-1

def is_huge(n, power):
    return any([int(c)>limits[len(str(n))] for c in str(n)])
    
def is_sum_of_digits(n, power):
    return n == sum([int(c)**power for c in str(n)])
    
limits = {d:find_upper(d, POWER) for d in range(2, find_max_digits(POWER)+1)}
'''
print(sum([n for d in limits\
                for n in range(BASE**(d-1),BASE**d) if\
                    #not is_huge(n, POWER) and \
                    is_sum_of_digits(n, POWER)]))

print(sum([n  for n in range(10, 1000000) if\
                    #not is_huge(n, POWER) and \
                    is_sum_of_digits(n, POWER)]))
'''             

print(sum([n for n in range(2,BASE**find_max_digits(POWER)) if\
                    is_sum_of_digits(n, POWER)]))