def prime(n):
    return [x for x in range(2, n+1) if all((x%y for y in range(2,int(x**0.5)+1)))]
 
print(sum(prime(int(2e6))))