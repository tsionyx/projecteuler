def square_difference(n):
    r = range(n+1)
    return (int(n*(n+1)/2))**2 - sum([i**2 for i in r])
 
print(square_difference(100))