def digits_sum(n):
    return sum(int(c) for c in str(n))

print(max(digits_sum(a**b) for a in range(1,100) for b in range (1,100)))