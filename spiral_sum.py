'''
	21 22 23 24 25
	20  7  8  9 10
	19  6  1  2 11
	18  5  4  3 12
	17 16 15 14 13
	
	S_D(5) = 101
	S_D(1001) = ?
'''

def diagonal_sum(n):
	if n == 1:
		return 1
	return 4*n**2 - 6*(n-1) + diagonal_sum(n-2)

print(diagonal_sum(1001))