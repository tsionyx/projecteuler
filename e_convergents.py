from fractions import Fraction
def get_convergents(start, prec, val_gen):
    import itertools
    vals = list(itertools.islice(val_gen, prec-1))
    
    res=Fraction(0,1)
    for i in reversed(vals):
        res = 1/(i+res)
    return res + start
    
def gen_e():
    k = 1
    while True:
        yield 1
        yield 2*k
        yield 1
        k += 1
        
        
print(sum(int(c) for c in str(get_convergents(2, 100, gen_e()).numerator)))