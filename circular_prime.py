cache = {}
def prime(n):
    if n < 2:
        return False
    if n in cache:
        return cache[n]
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    cache[n] = is_prime
    return is_prime
    
def rotations(number):
    s = str(number)
    return tuple(set([int(s[i:]+s[:i]) for i in range(len(s))]))


print(len([1 for i in range(1000000) if all([prime(n) for n in rotations(i)])]))