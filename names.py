import csv
filename = 'names.txt'
with open(filename, 'r') as f:
    names = list(csv.reader(f))[0]

names.sort()

def name_value(name):
    return sum([(ord(l)-ord('A')+1) for l in name])
    
print(sum([(i+1)*name_value(names[i]) for i in range(len(names))]))