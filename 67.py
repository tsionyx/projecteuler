triangle = [[int(n) for n in s.split()] for s in open('triangle.txt').readlines()]

for row in range(len(triangle) - 2, -1, -1):
	for item in range(len(triangle[row])):
		triangle[row][item] += max((triangle[row + 1][item], triangle[row + 1][item + 1]))

print(triangle[0][0])