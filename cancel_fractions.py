from fractions import Fraction
from functools import reduce
from operator import mul

def cancel_digit(n,d):
    if n % 10 == d % 10:
        return Fraction(n//10, d//10)
    if n % 10 == d // 10:
        return Fraction(n//10, d%10)
    if n // 10 == d % 10:
        return Fraction(n % 10, d//10)

print(reduce(mul,[Fraction(n,d) for d in range(11,101) for n in range(11,d)\
                        if (d % 10) and cancel_digit(n,d)==Fraction(n,d)],1).denominator)