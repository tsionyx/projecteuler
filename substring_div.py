primes = [2,3,5,7,11,13,17]
def substring_divisible(n):
    s = str(n)
    return all(int(s[i+1:i+4]) % primes[i]==0 for i in range(len(primes)))
    
print(substring_divisible(1406357289))
from itertools import permutations
print(sum(i for i in(int(''.join([str(j) for j in i])) for i in permutations(range(10))) if i > 1000000 and substring_divisible(i)))