def divisors(num):
    return [x for x in range(1, num+1) if n%x==0]

def divisors2(num):
    res = []
    for x in range(1, int(num**0.5) + 1):
        if n%x==0:
            res.append(x)
            if x*x!=n:
                res.append(int(n/x))
    return res
    
def divisors_len(num):
    return len([x for x in range(1, num+1) if n%x==0])

def divisors_len2(num):
    res = 0
    for x in range(1, int(num**0.5) + 1):
        if n%x==0:
            res += (2 if (x**2 != n) else 1)
    return res  
    
i = 1
while(True):
    n = int(i*(i+1)/2)
    d = divisors_len2(n)
    #print("%d: %d" % (n, set(divisors(n))==set(divisors2(n))))
    #print("%d: %d" % (n, d))
    if d > 500:
        print(divisors(n))
        break
    i += 1