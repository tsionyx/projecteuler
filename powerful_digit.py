def max_digits():
    n = 1
    while True:
        if 10**((n-1)/n) > 9:
            return n-1
        n += 1

from math import ceil
print(sum(10 - ceil(10**((d-1)/d)) for d in range(1, max_digits()+1)))