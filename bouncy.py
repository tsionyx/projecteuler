def is_bouncy(n):
    s = str(n)
    return not (all(s[d] >= s[d+1] for d in range(len(s)-1)) or\
                all(s[d] <= s[d+1] for d in range(len(s)-1)))
                
bouncy_counter,start = 0, 101
threshold = 99 

while True:
    if is_bouncy(start):
        bouncy_counter += 1
    if bouncy_counter/start >= threshold / 100:
        print(start, bouncy_counter, bouncy_counter/start)
        break
    start += 1
