from fractions import Fraction

l,h = Fraction(1/3),Fraction(1/2)

counter = 0
for d in range(4, 12000+1):
    print(d)
    for n in range(int(d*h)+1, int(d*l)-1, -1):
        cand = Fraction(n,d)
        if cand.denominator==d and l < cand < h:
            counter += 1
print(counter)
