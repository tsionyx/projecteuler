def fib(max):
    a,b = (1,1)
    while (a<max):
        yield a
        a, b = b, a + b
        
print(sum([x for x in fib(4000000) if x % 2 == 0]))