days = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)

def days_in_month(month, year):
    if is_leap(year) and month == 1:
        return 29
    return days[month]

def is_leap(year):
    if year % 100 == 0:
        return year % 400 == 0
    return year % 4 == 0
    
def days_in_year(year):
    #return 365 + (1 if is_leap(year) else 0)
    return sum([days_in_month(m, year) for m in range(len(days))])

def year_first_dayweek(year):
    return (sum([days_in_year(y) for y in range(1900, year)])+1) % 7

def month_first_dayweek(month, year):
    return (sum([days_in_month(m, year) for m in range(month)]) + year_first_dayweek(year))%7
    

#print(year_first_dayweek(1974))
#print(month_first_dayweek(4, 2014))
print(sum([1  for y in range(1901, 2001) for m in range(len(days)) if month_first_dayweek(m,y)==0]))