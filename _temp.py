loops = {1:1,2:1,145:1,45580:1, 
         169:3, 133660:3, 1445:3,
         178:2, 13456:2,
         278:2, 23456:2}
loopers = set([1,2,145,40585,169,363601,1454,871,45361,872,45362])

from math import factorial
df = [factorial(i) for i in range(10)]
     
def zero_pref(s):
    if s=='0':
        return 100
    return ord(s[0])
def fact_chain_rec(n):
    m = n
    n = int(''.join(sorted(str(n),key=zero_pref)))
    if m not in loopers and n in loops:
        return loops[n]+1
    if n in loops:
        return loops[n]
    if n not in loops:
        loops[n] = fact_chain_rec(sum(df[int(c)] for c in str(n)))
        return loops[n]+1
   
print(sum(1 for i in range(3,1000000) if fact_chain_rec(i)==60))