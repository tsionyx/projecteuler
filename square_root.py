from fractions import Fraction
def get_square_approx(prev=Fraction(1)):
	return Fraction(prev.numerator + 2*prev.denominator, prev.numerator + prev.denominator)

MAX = 1000
p = get_square_approx()
count = 0
for i in range(1,MAX):
	p = get_square_approx(p)
	if len(str(p.numerator)) > len(str(p.denominator)):
		count += 1
print(count)
	
	