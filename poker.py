def card_sort(card):
    return rank_sort(card[0])
    
def rank_sort(rank):
    return {
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
        '6': 6,
        '7': 7,
        '8': 8,
        '9': 9,
        'T': 10,
        'J': 11,
        'Q': 12,
        'K': 13,
        'A': 14,
    } [rank]

def factor(hand):
    ranks = [card[0] for card in hand]
    return sorted([(x, ranks.count(x)) for x in set(ranks)], key = card_sort)
    
def highest(hand):
    return factor(hand)[-1][0]
   
def hand_sort(hand):
    if is_royal_flush(hand):
        return 500
    if is_straight_flush(hand):
        return 480 + rank_sort(highest(hand))
    if is_four_of_a_kind(hand):
        return 460 + rank_sort([f[0] for f in factor(hand) if f[1]==4][0])
    if is_full_house(hand):
        return 440 + rank_sort([f[0] for f in factor(hand) if f[1]==3][0])
    if is_flush(hand):
        return 420 + rank_sort(highest(hand))
    if is_straight(hand):
        return 400 + rank_sort(highest(hand))
    if is_three_of_a_kind(hand):
        return 380 + rank_sort([f[0] for f in factor(hand) if f[1]==3][0])
    if is_two_pairs(hand):
        return 360 + rank_sort([f[0] for f in factor(hand) if f[1]==2][0])
    if is_one_pair(hand):
        return 340 + rank_sort([f[0] for f in factor(hand) if f[1]==2][0])
    else:
        return rank_sort(highest(hand))

    
def is_royal_flush(hand):
    return is_straight_flush(hand) and \
           highest(hand)=='T'

def is_straight_flush(hand):
    return is_straight(hand) and is_flush(hand)
    
def is_four_of_a_kind(hand):
    return any(rank[1]==4 for rank in factor(hand))
    
def is_full_house(hand):
    return any(rank[1]==3 for rank in factor(hand)) and\
           any(rank[1]==2 for rank in factor(hand))
           
def is_flush(hand):
    return len(set(card[1] for card in hand))==1
    
def is_straight(hand):
    f = factor(hand)
    return len(f)==5 and\
           all(rank_sort(f[i][0])+1==rank_sort(f[i+1][0]) for i in range(len(f)-1))

def is_three_of_a_kind(hand):
    return any(rank[1]==3 for rank in factor(hand))
    
def is_two_pairs(hand):
    return [rank[1] for rank in factor(hand)].count(2)==2
    
def is_one_pair(hand):
    return any(rank[1]==2 for rank in factor(hand))
    
def untie(hand1, hand2):
    return 1 if rank_sort(highest(hand1))>rank_sort(highest(hand2)) else 2
        
    
with open('poker.txt') as hands:
    hands = [line.strip().split() for line in hands.readlines()]
    
hands = tuple((tuple(hand[:5]), tuple(hand[5:])) for hand in hands)

res = 0
for round in hands:
    val1, val2 = hand_sort(round[0]), hand_sort(round[1])
    if val1 > val2:
        res += 1
        print(round[0], round[1], '--> 1 ', val1, val2 )
    elif val1 < val2:
        print(round[0], round[1], '--> 2 ', val1, val2 )
    else:
        if untie(round[0], round[1]) == 1:
            res += 1
        print(round[0], round[1], '--> ',untie(round[0], round[1]), val1, val2 )
print('%d/%d' % (res, len(hands)))