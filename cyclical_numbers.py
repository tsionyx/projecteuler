def get_ranges(figure):
    for i in range(1,10000):
        if figure(i-1) < 1000 and figure(i) >= 1000:
            l = i
        if figure(i) >= 10000:
            return [figure(i) for i in range(l,i)]                
    
figures = {3: get_ranges(lambda x: x*(x+1)//2),
           4: get_ranges(lambda x: x**2),
           5: get_ranges(lambda x: x*(3*x-1)//2),
           6: get_ranges(lambda x: x*(2*x-1)),
           7: get_ranges(lambda x: x*(5*x-3)//2),
           8: get_ranges(lambda x: x*(3*x-2))}

'''
for n in figures:
    figure = figures[n]
    for number in figure[::]:
        d,m = divmod(number, 100)
        if m < 10 or \
           all((j % 100 != d) and (j // 100 != m) for i in figures for j in figures[i] if i!=n):
            #print(n,number)
            figure.remove(number)
'''

def get_chain(root, start, used):
    if len(used)==6:
        if start % 100 == root // 100:
            return [start]
        else:
            return []
    #print(root, start, used)        
    for n in figures.keys() - used:
        for number in figures[n]:
            if start % 100 == number // 100:
                chain = get_chain(root, number, used+[n])
                if chain:
                    return [start] + chain
    return []
print(sum(max([get_chain(i, i, [8]) for i in figures[8]])))