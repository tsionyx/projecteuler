def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    return is_prime

prod,i = 1,2
while True:
    if prime(i):
        prod *= i
        if prod > 1000000:
            print(prod//i)
            break
    i += 1
    
def factor(N):
    assert N > 1, "What r u going 2 do?"
    
    factors = {}
    n = N
    while(n>1):
        found = False
        for y in range(2,int(N**0.5)+1):
            if n%y == 0:
                found = True
                if y in factors:
                    factors[y] += 1
                else:
                    factors[y] = 1
                n //= y
                break
        if not found:
            factors[n]=1
            break
    return factors    
def totient(n):
    for f in tuple(factor(n)):
        n = n//f * (f-1)
    return n        
#print(max(((i, i/totient(i)) for i in range(2,1000000+1)), key = lambda x: x[1]))