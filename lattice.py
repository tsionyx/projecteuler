# all paths are can be written as sequence of n 'R' and n 'D': 'RRDD'
def perm_mult(n):
    from math import factorial
    #http://en.wikipedia.org/wiki/Permutation#Permutations_of_multisets
    return int(factorial(2*n)/(factorial(n)**2))

print(perm_mult(20))