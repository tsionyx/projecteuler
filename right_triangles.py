'''
    p = a + b + c
    a**2 + b**2 = c**2
    p**2 = a**2 + b**2 + c**2 + 2*(a*b+b*c+c*a) = 2(c**2+a*b+b*c+c*a) => p % 2 == 0
    
    p = a + b + c
    c > a, c > b => c > p // 3
    a + b > c => c < p // 2
    
    for given p and c:
        a + b = p - c
        a*b = ((a + b)**2 - (a**2 + b**2)) / 2 = ((p - c)**2 - c**2) / 2 
    
'''


def solve_quadratic_equation(p, q):
    ''' p is sum, q is product of roots '''
    d =  p**2 - 4*q
    if d < 0:
        return None
    return ((p+d**.5)/2,(p-d**.5)/2) 
 
from time import time
start_time = time()


MAX = 1000
res = {}
for p in range(12, MAX+1, 2):
    #sol = []
    sol = 0
    for c in range(p//3+1, p//2):
        roots = solve_quadratic_equation(p-c, ((p - c)**2 - c**2) // 2)
        if roots:
            if int(roots[0])**2 + int(roots[1])**2 == c**2:
                #sol.append(roots)
                sol += 1
    if sol:
        res[p] = sol
        
import operator
#print(max({k: len(res[k]) for k in res}.items(), key=operator.itemgetter(1))[0])
print(max(res.items(), key=operator.itemgetter(1))[0])


print(time() - start_time, "seconds") 
