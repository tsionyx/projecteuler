'''n^2 + an + b, where |a| < 1000 and |b| < 1000
Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
'''


''' |b| is prime '''

cache = {}
def prime(n):
    if n < 2:
        return False
    if n in cache:
        return cache[n]
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    cache[n] = is_prime
    return is_prime

def len_prime_sequence(a,b):
    n = 0
    while True:
        if not prime(n**2 + a*n + b):
            break
        n = n + 1
    return n
  
from time import time
start_time = time()

   
res = max([(a, b, len_prime_sequence(a,b)) for b in range(-1000, 1001) for a in range(-1000, 1001) if prime(abs(b))], key=lambda x: x[2])

print(res[0]*res[1])

print(time() - start_time, "seconds") 

