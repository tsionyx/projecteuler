'''145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
'''
from math import factorial

def find_max_digits():
    f = factorial(9)
    digits = 2
    
    while(f*digits >= 10**(digits)):
        digits += 1
    return digits

def curious(n):
    return n == sum([factorial(int(c)) for c in str(n)])

print(sum([i for i in range(10, factorial(9)*find_max_digits()+1) if curious(i)]))


