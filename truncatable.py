cache = {}
def prime(n):
    if n < 2:
        return False
    if n in cache:
        return cache[n]
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    cache[n] = is_prime
    return is_prime
    
def is_truncatable_prime(n):
    s = str(n)
    return prime(n) and\
           all([prime(int(s[i:])) and prime(int(s[:i])) for i in range(1, len(s))])

#print(is_truncatable_prime(3797))
    
i = 23
sum, counter = 0, 0
while(True):
    if is_truncatable_prime(i):
        counter += 1
        sum += i
        #print("%d. i = %d, sum = %d" % (counter, i,sum))
        if(counter == 11):
            print(sum)
            break
    i += 2    
    