def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    return is_prime

from itertools import permutations

res = {}
for i in range(1000,10000):
    if prime(i):
        perm = set(int("".join([c for c in d])) for d in list(permutations(str(i))))
        candidates = sorted(d for d in perm if prime(d) and d>1000)
        if len(candidates)>2:
            res[i] = tuple(candidates)

            
for d in res:
    n = res[d]
    for i in range(len(res[d])-2):
        diff = n[i+1] - n[i]
        if n[i+1]+diff in n:
            print(n[i], n[i] + diff, n[i] + 2*diff) 
      