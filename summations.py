numbers = range(1,101)

cache = {}
def get_variants(total):
    if total in cache:
        return cache[total]
    res = []
    for p in numbers:
        if p == total:
            res.append((p,))
        elif p < total:
            for d in get_variants(total - p):
                res.append(tuple([p] + list(d)))
                
    res = tuple(set(tuple(sorted(s)) for s in res))
    cache[total] = res
    #print(total, len(res))
    return res
    
#[print(i, len(get_variants(i))-2) for i in range (1,100)]
#print((get_variants(6)))


target =100
ways=[1]+[0]*(target)

for i in range(1,target):
    for j in range(i, target+1):
        ways[j] += ways[j-i]
        
print(ways[-1])