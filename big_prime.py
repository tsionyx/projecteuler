def prime(n):
    return [x for x in range(2, n+1) if all((x%y for y in range(2,int(x**0.5)+1)))]

n = 10001
# According to http://en.wikipedia.org/wiki/Prime_number_theorem#Approximations_for_the_nth_prime_number 
from math import log
q = n*(log(n)+log(log(n)))

print(prime(int(q))[n-1])