def sieve(max):
    if max < 3:
        raise ValueError("Use sieve with at least 3 as maximum number")
    res = [2]
    for n in range(3,max):
        if all(n%y for y in range(2,int(n**0.5)+1) if y in res):
            res.append(n)            
    return res
    
if __name__ == '__main__':
    print(sieve(1000))