def is_pentagonal(number):
    test = ((1 + 24 * number)**.5 + 1)/6
    return test == int(test)

def min_dif_pent(): 
    i = 1
    while True:
        n = i * (3*i-1) // 2
        for j in range(i-1, 0, -1):
            m = j * (3*j-1) // 2
            if is_pentagonal(n+m) and is_pentagonal(n-m):
                return n-m
        i += 1
                    
print(min_dif_pent())