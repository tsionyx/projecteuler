with open('base_exp.txt') as exps:
    numbers = tuple(map(int,line.strip().split(',')) for line in exps.readlines())
    
from math import log    
logs = [exp*log(base) for base,exp in numbers]

print(max(enumerate(logs), key=lambda x: x[1])[0]+1)