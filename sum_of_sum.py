'''
	S(N)=sum(i=1..N,sum(j=1..N, d(i*j)))
	S(3) = 1,2,3,2,4,6,3,6,9 = 59
	S(10^3) = 563576517282
	S(10^5) % 10^9 = 215766508
	S(10^11) $ 10^9
'''
cache = {}
cache_hit = 0

def divisors(n):
	global cache_hit
	if n in cache:
		cache_hit += 1
		return cache[n]
	res = []
	for x in range(1, int(n**0.5)+1):
		if n%x == 0:
			res.append(x)
			if  x**2 != n:
				res.append(int(n/x))
	res.sort()
	t = tuple(res)
	cache[n] = t
	return t
	
def d(n):
	return sum(divisors(n))

	
def huge_sum(n):
	return sum([d(i*j) for i in range(1,n+1) for j in range(1,n+1)])
	
[print(i, huge_sum(i)) for i in range(1000)]