def sum_digits(n):
    s = 0
    while n:
        n, r = divmod(n, 10)
        s += r
    return s
import timeit
   
print(sum_digits(2**1000))
timeit.timeit("sum_digits(2**1000)")