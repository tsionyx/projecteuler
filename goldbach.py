def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    return is_prime
    
def goldbach(n):
    for i in range(n-1):
        if prime(i):
            possible_square = (n - i)//2
            if int(possible_square**.5)**2 == possible_square:
                return (i, int(possible_square**.5))
    return None
                
odd = 3
while True: 
    if not prime(odd):
        if not goldbach(odd):
            print(odd)
            break
    odd += 2
    