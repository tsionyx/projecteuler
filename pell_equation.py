def is_integer(f):
    return round(f) == f
  
def sqrt(N):
    n=N**.5
    integer = int(n)
    fractions = []
    start = [0]*3
    start[2] = N - integer**2
    if start[2] == 0:
        return [integer, []]
    start[0] = int((n + integer)/start[2])
    start[1] = start[0]*start[2] - integer
    fractions.append(start[0])
    
    i, r, d = start
    while True:
        d = (N - r**2) // d
        i = int((n + r)/d)
        r = i*d - r
        if [i,r,d] == start:
            break
        fractions.append(i)
    
    return [integer, fractions]
    
    
from fractions import Fraction
def get_pell_sol(N):
  sq = sqrt(N)
  
  res = Fraction(0)
  for i in sq[1][-2::-1]:
    res = 1/(i+res)
    
  if len(sq[1]) % 2:
    for i in sq[1][-1::-1]:
      res = 1/(i+res)
      
  return (lambda x: (x.numerator, x.denominator))(res + sq[0])
 

print(max(((i, get_pell_sol(i)) for i in range(1,1000) if not is_integer(i**.5)), key = lambda x: x[1][0])[0])