pences = (1, 2, 5, 10, 20, 50, 100, 200)

cache = {}
def get_variants(total):
    if total in cache:
        return cache[total]
    res = []
    for p in pences:
        if p == total:
            res.append((p,))
        elif p < total:
            for d in get_variants(total - p):
                res.append(tuple([p] + list(d)))
                
    #res = set(map(tuple, res))
    #print("Variants(%d)= %r" % (total, res))
    res = tuple(set(tuple(sorted(s)) for s in res))
    cache[total] = res
    return res
    
print(len(get_variants(200)))
