'''
	12 is the smallest abundant as 1 + 2 + 3 + 4 + 6 = 16 > 12 itself
	24 is the smallest number represented as the sum of two abundant 24 = 12 + 12
	all N > 28123 can be represnted as the sum of abundant
	
	Problem:
		Find the sum of all numbers that can't be written as the sum of two abundant.
	Steps:
		1. Find all the abundant N <= 28123. Sort it acsendant.
		2. Foreach N <= 28123 try to write it as the sum of two abundant

		
'''
cache = {}
cache_hit = 0

def divisors(n):
	global cache_hit
	if n in cache:
		cache_hit += 1
		return cache[n]
	res = []
	for x in range(1, int(n**0.5)+1):
		if n%x == 0:
			res.append(x)
			if 1 < x**2 < n:
				res.append(int(n/x))
	res.sort()
	t = tuple(res)
	cache[n] = t
	return t

def is_abundant(n):
	if n<=0:
		return False
	return sum(divisors(n)) > n
	
limit = 28123
abundants = tuple([i for i in range(1, limit+1) if is_abundant(i)])

def is_abundant_sum(n):
	return any([is_abundant(n-i) for i in abundants if i<n])

non_abundant_sum = 0
for i in range(limit+1):
	if not is_abundant_sum(i):
		non_abundant_sum += i
		print(i, non_abundant_sum)
		
print("Sum is %d. Cache hit is %d" % (non_abundant_sum, cache_hit))	
#print(sum([i for i in range(limit+1) if not is_abundant_sum(i)]))
