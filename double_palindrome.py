def is_palindrome(num):
    s = str(num)
    return s == s[::-1]
   
def to_binary(n):
    return "{0:b}".format(n)
    
def is_double_palindrome(n):
    return is_palindrome(n) and is_palindrome(to_binary(n))
    
print(sum([i for i in range(1000000) if is_double_palindrome(i)]))