def collatz(start):
    next = start
    while(next != 1):
        yield next
        next = 3 * next + 1 if next % 2 else int(next / 2)
    yield 1
        
l = [(len(list(collatz(i))),i) for i in range(2,int(1e5))]

max_i, max_l = 2, 2
for i in l:
    if i[0]>max_l:
        max_l = max(max_l, i[0])
        max_i = i[1]

print(max_i)
    