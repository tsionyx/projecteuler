'''
def get_triangle():
    i = 1
    while(True):
        yield i*(i+1) // 2
        i += 1
        
def get_pentagonal():
    i = 1
    while(True):
        yield i*(3*i-1) // 2
        i += 1
        
[print(get_hexagonal(i), get_triangle(2*i-1)) for i in range(100)]
''' 

x, y = 0, 0       
def get_triangle():
    global x
    x += 1
    return x*(x+1) // 2
        
def get_pentagonal():
    global y
    y += 1
    return y*(3*y-1) // 2

tr, pent = get_triangle(), get_pentagonal()


while(True):
    if tr < pent:
        tr = get_triangle()
    elif tr > pent:
        pent = get_pentagonal()
    else:
        # more than given and x is odd
        if tr > 40755 and x % 2:
            print(x, y, (x+1)//2, tr)
            break
        tr = get_triangle()
        pent = get_pentagonal()