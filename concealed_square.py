for i in range (10**8, int(10**8*(2**.5))):
    if all((i**2) // 10**(18-2*d) % 10 == d for d in range(2,10)):
        print(i*10)
        break