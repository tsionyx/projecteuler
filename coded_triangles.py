def find_triangles(max):
    tr, i, res = 0, 1, []
    while(True):
        tr = i*(i+1) // 2
        if(tr > max):
            break;
        res.append(tr)
        i += 1
    return tuple(res)
    
def word_value(word):
    return sum([(ord(l)-ord('A')+1) for l in word])
    
    
import csv
filename = 'words.txt'
with open(filename, 'r') as f:
    words = list(csv.reader(f))[0]
   
max_len = max(map(len,words))
max_letter = 26

tr = find_triangles(max_letter*max_len)
print(len([word for word in words if word_value(word) in tr]))
