 
from time import time
start_time = time()
from itertools import groupby
cubes = []
i = 1
while True:
    found = False
    cubes += [(i, i**3) for i in range(i, i+1000)]
    sorted_cubes = sorted([(n, ''.join(sorted(str(cube)))) for n, cube in cubes], key = lambda x: x[1])
    for k, g in  groupby(sorted_cubes, lambda x: x[1]):
        l = list(g)
        if len(l) >= 5:
            print(l[0][0]**3)
            found = True
            break
    if found:
        break
    i+=1000
print(time() - start_time, "seconds") 