cache={}
def divisors(n):
    if n in cache:
        return cache[n]
    res = []
    for x in range(1, int(n**0.5) + 1):
        if n%x==0:
            res.append(x)
            if (x*x!=n) and x!=1:
                res.append(int(n/x))
                
    cache[n] = tuple(res)
    return res

def is_amicable(n, m):
    # if this is True then number a=b called a perfect number
    if n == m:
        return False
        
    return sum(divisors(n)) == m and \
           sum(divisors(m)) == n   

max = 10000
l = [(a,b) for a in range(1, max) for b in range(a+1, max) if is_amicable(a,b)]

print(l)
print(sum([i[0]+i[1] for i in l]))