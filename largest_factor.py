#def prime(max):
#    return [x for x in range(2, max+1) if all((x%y for y in range(2,1+int(x**0.5))))]
#def largest_factor(num):
#    return max([x for x in prime(num) if not (num % x)]);

def largest_factor(num):
    d = 2
    while d < num:
        while num % d == 0:
            num /= d
        d += 1
    return d

print (largest_factor(600851475143))