cache = {}
def arrive_at_89(n, build_cache = False):
    if n in cache:
        return cache[n]
    if n == 89:
        return True
    if n == 1:
        return False
    m = sum(int(c)**2 for c in str(n))
    res = arrive_at_89(m)
    if build_cache:
        cache[m] = res
        cache[n] = res
    return res

def build_cache(m):
    for i in range(2, 81*(len(str(m))-1)+1):
        arrive_at_89(i, True)
    print(cache)

build_cache(10000000)
print(sum(1 for n in range(2,10000000) if arrive_at_89(n)))