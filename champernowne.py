def find_limit(l):
    ''' How many numbers we need to write in a row 
    to get a sequence with specified length [l]?'''
    # total amount of digits
    digits = 0
    # the final number to get sequence with length [l]
    number = 0
    ten_power = 1
    while(digits < l):
        #count of numbers that have ten_power digits
        addition = 9*10**(ten_power-1)
        # when we add [addition] numbers with [ten_power] digits in each...
        number += addition
        # ...the sequence getting bigger on [ten_power*addition] digits
        digits += ten_power*addition
        ten_power += 1
    return number - (digits - l) // (ten_power-1) 

s = '0'
# our sequence should be 1000000+1 digits long to get 1000000th element
for n in range(1,find_limit(1000000+1)+1):
    s += str(n)
print(len(s))
print(int(s[1])*int(s[10])*int(s[100])*int(s[1000])*int(s[10000])*int(s[100000])*int(s[1000000]))
