def prime(n):
    if n < 2: return False
    return all((n%y for y in range(2,int(n**0.5)+1)))
    
from itertools import combinations
def get_families(n, l):
    N = len(str(n))+l
   
   # 10 possible 
    ten_family = list(combinations(range(1,N), l))
    
    # 9 possible (all but 0)
    nine_family = list(tuple([0]+list(c)) for c in combinations(range(1,N), l-1))
    
    res = []
    for c in ten_family:
        res.append([int(insert_in_string(str(n), str(repl), c))  for repl in range(10)])
    for c in nine_family:
        res.append([int(insert_in_string(str(n), str(repl), c))  for repl in range(1,10)])
    return res
        
def insert_in_string(s, repl, places):
    for place in places:
        s = s[:place] + repl + s[place:]
    return s
    
def contains_primes(family, n):
    return sum(1 for num in family if prime(num)) == n

def solve(N):    
    sum_len = 2 
    while True:
        for digits in range(1,sum_len):
            for i in range(10**(digits-1), 10**digits):
                families = get_families(i, sum_len-digits)
                for family in families:
                    if contains_primes(family, N):
                        return i, family
        sum_len += 1

 
from time import time
start_time = time()       
print(solve(8))
print(time() - start_time, "seconds") 