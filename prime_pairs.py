cache = {}
def prime(n):
    if n < 2:
        return False
    if n < 4:
        return True
    if n in cache:
        return cache[n]
    is_prime = all((n%y for y in range(2,int(n**0.5)+1)))
    cache[n] = is_prime
    return is_prime
    
def sieve(max):
    if max < 3:
        raise ValueError("Use sieve with at least 3 as maximum number")
    res = [2]
    for n in range(3,max):
        if all(n%y for y in range(2,int(n**0.5)+1) if y in res):
            res.append(n)            
    return res
    
from itertools import permutations    
def criteria(chain):
    # return all(prime(n) for n in chain) and \
    return all(prime(p[0]*10**(len(str(p[1]))) + p[1]) for p in permutations(chain,2))
 
def solve(N):
    def get_chain(chain, target):
        if len(chain)==target:
            return chain 
        maximum = chain[-1]
        for n in primes:
            if n <= maximum:
                continue
            if criteria(chain+[n]):
                candidate = get_chain(chain+[n],target)
                if candidate:
                    return candidate
        return []  
        
    i = 1
    while True:
        primes = sieve(i*10000)
        for p in primes:
            ch = get_chain([p], N)
            if ch:
                return ch
        i += 1

print(sum(solve(5)))