with open("matrix.txt") as m_file:
    matrix = [list(map(int,line.strip().split(','))) for line in m_file.readlines()]

from time import time
start_time = time()

N = len(matrix)

matrix = [list(map(lambda x: [0,x],row)) for row in matrix]

for i, row in enumerate(matrix):
    for j, cell in enumerate(row):
        if j < N - 1:
            if matrix[i][j+1][0] == 0:
                matrix[i][j+1][0] = cell[0]+cell[1]
            matrix[i][j+1][0] = min(matrix[i][j+1][0], cell[0]+cell[1])
        if i < N - 1:
            if matrix[i+1][j][0] == 0:
                matrix[i+1][j][0] = cell[0]+cell[1]
            matrix[i+1][j][0] = min(matrix[i+1][j][0], cell[0]+cell[1])
        
print(matrix[-1][-1][0] + matrix[-1][-1][1])

print(time() - start_time, "seconds") 