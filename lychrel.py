def is_palindrome(n):
    s = str(n)
    return s == s[::-1]
    
def is_non_lychrel(n):
    i = 1
    while True:
        if i > 50:
            return False
        n += int(str(n)[::-1])
        if is_palindrome(n):
            return True #(n,i)
        i += 1

print(len([i for i in range(1,10000) if not is_non_lychrel(i)]))